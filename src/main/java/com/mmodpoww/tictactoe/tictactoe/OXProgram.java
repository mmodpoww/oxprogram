/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmodpoww.tictactoe.tictactoe;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OXProgram {

    static char winner = '-';
    static boolean isFinish = false;
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    static int countturn = 0;
//    static int countturno = 0;
    static char player = 'X';
    static int row, col;
    static Scanner kb = new Scanner(System.in);

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println("1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " Turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            if (countturn >= 8) {
                break;
            }
            System.out.println("Error : Table at row and col is not empty!!!");
        }
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[j][j] != player) {
                    return;
                }
            }
        }
        isFinish = true;
        winner = player;
    }
    static void checkY(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0, k = 2; j < 3; j++, k--) {
                if (table[j][k] != player) {
                    return;
                }
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkY();
        checkDraw();
    }

    static void checkDraw() {
        if (countturn >= 8) {
            winner = 'D';
            isFinish = true;
        }

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
        countturn++;
    }

    static void showResult() {
        if (winner == 'D') {
            System.out.println("Draw");
        } else {
            System.out.println(winner + "Win!!");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye...");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showTable();
        showResult();
        showBye();
    }
}
